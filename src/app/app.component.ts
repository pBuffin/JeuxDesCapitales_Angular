import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  
   styles: [`
   
   #images {
     display: inline-block;
    white-space: nowrap;
}
   
   `],
  
template:`  <div> 
                <img class="col-md-12" src="assets/header.png"/>
            </div>

            <div class="well bs-component">
                <form class="form-horizontal">
                    <div class="col-md-offset-4">  
                        <h3>Quelle est la capitale {{questions[rand].quest}}</h3>  
                    </div>
                    <div class="row">
                      <div class="col-md-offset-4">  
                        <input type="button" class="btn btn-raised btn-primary" value="{{questions[rand].rep1}}" (click)="onSelect(questions[rand],'A')"/>      
                        <input type="button" class="btn btn-raised btn-primary" value="{{questions[rand].rep2}}" (click)="onSelect(questions[rand],'B')"/>     
                        <input type="button" class="btn btn-raised btn-primary" value="{{questions[rand].rep3}}" (click)="onSelect(questions[rand],'C')"/>
                        <input type="button" class="btn btn-raised btn-primary" value="{{questions[rand].rep4}}" (click)="onSelect(questions[rand],'D')"/> 
                         <div  id="images">                     
                            <div [ngSwitch]="ok">
                                <div *ngSwitchCase="'none'">       
                            </div>               
                                <div *ngSwitchCase="'correct'">
                                    <img src="assets/vrai.png" />
                                </div>
                                <div *ngSwitchCase="'incorrect'">
                                    <img src="assets/faux.png" />
                                </div>
                            </div>
                            </div>
                        </div>    
                    </div>  
                    <div class="row">
                        <div class="col-md-offset-4">
                         <h3> Votre Score : {{score}}</h3>
                         </div>
                    </div>
                </form>
            </div>
`
  
})
export class AppComponent {
  title = 'app';
  rand = Math.floor((Math.random() * 9) + 1);
  questions= QUESTIONS;
  score=0;
  ok='none';
  
    onSelect(question : Question,letter: string){
        if (question.resp == letter){
            this.ok = 'correct';
            this.score = this.score + 1;
        }else{
             this.ok = 'incorrect';
        }
   function sleep (time) {
        return new Promise((resolve) => setTimeout(resolve, time));
}

// Usage!
sleep(1000).then(() => {
    this.ok='none'
        this.rand = Math.floor((Math.random() * 71) + 1);
});  
  
  }
}

export class Question{

    id: number;
    quest: string;
    rep1: string;
    rep2: string;
    rep3: string;
    rep4: string;
    resp: string;
    
}

const QUESTIONS: Question[] = [
      { id: 1, quest: ' de l Albanie?', rep1: 'Manama', rep2: 'Tirana', rep3: 'Vienne', rep4: 'Minsk',resp:'B'},
      { id: 2, quest: ' de l Algérie?', rep1: 'Zagreb', rep2: 'Moroni', rep3: 'Alger', rep4: 'Séoul',resp:'C'},
      { id: 3, quest: ' de l Allemagne?', rep1: 'Berlin', rep2: 'Chisinau', rep3: 'Islamabad', rep4: 'Honiara',resp:'A'},
      { id: 4, quest: ' de l Angola?', rep1: 'Koweït', rep2: 'Kingston', rep3: 'Palikir', rep4: 'Luanda',resp:'D'},
      { id: 5, quest: ' de l Arabie saoudite?', rep1: 'Asuncion', rep2: 'Riyad', rep3: 'Montevideo', rep4: 'Paramaribo',resp:'B'},
      { id: 6, quest: ' de l Argentine?', rep1: 'Buenos Aires', rep2: 'Moscou', rep3: 'Doha', rep4: 'Tachkent',resp:'A'},
      { id: 7, quest: ' de l Australie?', rep1: 'Katmandou', rep2: 'Lilongwe', rep3: 'Canberra', rep4: 'Monrovia',resp:'C'},
      { id: 8, quest: ' de l Autriche?', rep1: 'Port-Louis', rep2: 'Bucarest', rep3: 'Prague', rep4: 'Vienne',resp:'D'},
      { id: 9, quest: ' de l Azerbaïdjan?', rep1: 'Paramaribo', rep2: 'Bakou', rep3: 'Bratislava', rep4: 'Kigali',resp:'B'},
      { id: 10, quest:' du Bangladesh?', rep1: 'Dacca', rep2: 'Wellington', rep3: 'Tachkent', rep4: 'Achgabat',resp:'A'},
      { id: 11, quest:' de la Belgique?', rep1: 'Castries', rep2: 'Apia', rep3: 'Honiara', rep4: 'Bruxelles',resp:'D'},
      { id: 12, quest:' de la Biélorussie?', rep1: 'Prague', rep2: 'Nuku alofa', rep3: 'Minsk', rep4: 'Harare',resp:'C'},
      { id: 13, quest:' de la Birmanie?', rep1: 'Naypyidaw', rep2: 'Santiago', rep3: 'Copenhague', rep4: 'San José',resp:'A'},
      { id: 14, quest:' de la Bolivie?', rep1: 'Kigali', rep2: 'Londres', rep3: 'Varsovie', rep4: 'La Paz',resp:'D'},
      { id: 15, quest:' de la Bosnie-Herzégovine?', rep1: 'Tallinn', rep2: 'Sarajevo', rep3: 'Asmara', rep4: 'Budapest',resp:'B'},
      { id: 16, quest:' du Botswana?', rep1: 'Rome', rep2: 'Zagreb', rep3: 'Gaborone', rep4: 'Moroni',resp:'C'},
      { id: 17, quest:' du Brésil?', rep1: 'Helsinki', rep2: 'Bichkek', rep3: 'Mexico', rep4: 'Brasilia',resp:'D'},
      { id: 18, quest:' de la Bulgarie?', rep1: 'Sofia', rep2: 'Niamey', rep3: 'Kiev', rep4: 'Vilnius',resp:'A'},
      { id: 19, quest:' du Burkina Faso?', rep1: 'Asmara', rep2: 'Ouagadougou', rep3: 'Yamoussoukro', rep4: 'Lilongwe',resp:'B'},
      { id: 20, quest:' du Cambodge?', rep1: 'Skopje', rep2: 'Vaduz', rep3: 'Islamabad', rep4: 'Phnom Penh',resp:'D'},
      { id: 21, quest:' du Cameroun?', rep1: 'Kigali', rep2: 'Niamey', rep3: 'Yaoundé', rep4: 'Achgabat',resp:'C'},
      { id: 22, quest:' du Canada?', rep1: 'Ottawa', rep2: 'Bridgetown', rep3: 'Nassau', rep4: 'Sucre',resp:'A'},
      { id: 23, quest:' de la République centrafricaine?', rep1: 'Banjul', rep2: 'Bangui', rep3: 'Monrovia', rep4: 'Bissau',resp:'B'},
      { id: 24, quest:' du Chili?', rep1: 'Managua', rep2: 'Tegucigalpa', rep3: 'Roseau', rep4: 'Santiago',resp:'D'},
      { id: 25, quest:' de la Chine?', rep1: 'Pékin', rep2: 'Séoul', rep3: 'Tokyo', rep4: 'New Delhi',resp:'A'},
      { id: 26, quest:' de Chypre?', rep1: 'Sofia', rep2: 'Nicosie', rep3: 'Tallinn', rep4: 'Bruxelles',resp:'B'},
      { id: 27, quest:' de la Colombie?', rep1: 'Santiago', rep2: 'San José', rep3: 'Nicosie', rep4: 'Bogota',resp:'D'},
      { id: 28, quest:' de la Corée du Nord?', rep1: 'Séoul', rep2: 'Pékin', rep3: 'Pyongyang', rep4: 'Banjul',resp:'C'},
      { id: 29, quest:' Corée du Sud?', rep1: 'Beyrouth', rep2: 'Pékin', rep3: 'Pyongyang', rep4: 'Séoul',resp:'D'},
      { id: 30, quest:' de la Côte d Ivoire?', rep1: 'Yamoussoukro', rep2: 'Malabo', rep3: 'Bamako', rep4: 'Kigali',resp:'A'},
      { id: 31, quest:' de la Croatie?', rep1: 'Tirana', rep2: 'Zagreb', rep3: 'Tallinn', rep4: 'Pristina',resp:'B'},
      { id: 32, quest:' de Cuba?', rep1: 'Vilnius', rep2: 'Nassau', rep3: 'San José', rep4: 'La Havane',resp:'D'},
      { id: 33, quest:' du Danemark?', rep1: 'Oslo', rep2: 'Stockholm', rep3: 'Copenhague', rep4: 'Bucarest',resp:'C'},
      { id: 34, quest:' de l Égypte?', rep1: 'Le Caire', rep2: 'Maputo', rep3: 'Kinshasa', rep4: 'São Tomé',resp:'A'},
      { id: 35, quest:' des Émirats arabes unis?', rep1: 'Abuja', rep2: 'Abou Dabi', rep3: 'Niamey', rep4: 'Kampala',resp:'B'},
      { id: 36, quest:' de l Équateur?', rep1: 'Tripoli', rep2: 'Montevideo', rep3: 'Caracas', rep4: 'Quito',resp:'D'},
      { id: 37, quest:' de l Espagne?', rep1: 'Nicosie', rep2: 'Caracas', rep3: 'Madrid', rep4: 'Quito',resp:'C'},
      { id: 38, quest:' de l Estonie?', rep1: 'Vilnius', rep2: 'Varsovie', rep3: 'Bucarest', rep4: 'Tallinn',resp:'D'},
      { id: 39, quest:' des États-Unis?', rep1: 'Washington D.C.', rep2: 'Saint-Georges', rep3: 'Bruxelles', rep4: 'Pyongyang',resp:'A'},
      { id: 40, quest:' de l Éthiopie?', rep1: 'Windhoek', rep2: 'Addis-Abeba', rep3: 'Kigali', rep4: 'Dakar',resp:'B'},
      { id: 41, quest:' de la Finlande?', rep1: 'Helsinki', rep2: 'Copenhague', rep3: 'Dublin', rep4: 'Oslo',resp:'A'},
      { id: 42, quest:' de la France?', rep1: 'Moscou', rep2: 'Amsterdam', rep3: 'La Valette', rep4: 'Paris',resp:'D'},
      { id: 43, quest:' du Gabon?', rep1: 'Banjul', rep2: 'Niamey', rep3: 'Libreville', rep4: 'Kinshasa',resp:'C'},
      { id: 44, quest:' du Ghana?', rep1: 'Conakry', rep2: 'Accra', rep3: 'Thimbu', rep4: 'Tbilissi',resp:'B'},
      { id: 45, quest:' de Haïti?', rep1: 'Podgorica', rep2: 'Melekeok', rep3: 'Dili', rep4: 'Port-au-Prince',resp:'D'},
      { id: 46, quest:' du Honduras?', rep1: 'Tegucigalpa', rep2: 'Lima', rep3: 'Asunción', rep4: 'San José',resp:'A'},
      { id: 47, quest:' de la Hongrie?', rep1: 'Canberra', rep2: 'Vilnius', rep3: 'Budapest', rep4: 'Pristina',resp:'B'},
      { id: 48, quest:' l Inde?', rep1: 'Jakarta', rep2: 'Manama', rep3: 'New Delhi', rep4: 'Amman',resp:'C'},
      { id: 49, quest:' de l Irak?', rep1: 'Astana', rep2: 'Téhéran', rep3: 'Banjul', rep4: 'Bagdad',resp:'D'},
      { id: 50, quest:' de l Iran?', rep1: 'Téhéran', rep2: 'Mascate', rep3: 'Doha', rep4: 'Bagdad',resp:'A'},
      { id: 51, quest:' de l Irlande?', rep1: 'Bridgetown', rep2: 'Dublin', rep3: 'Ottawa', rep4: 'Reykjavik',resp:'B'},
      { id: 52, quest:' de l Italie?', rep1: 'Paris', rep2: 'Athènes', rep3: 'Rome', rep4: 'Dublin',resp:'C'},
      { id: 53, quest:' de la Jamaïque?', rep1: 'Amman', rep2: 'Nairobi', rep3: 'Vientiane', rep4: 'Kingston',resp:'D'},
      { id: 54, quest:' de la Lettonie?', rep1: 'Skopje', rep2: 'Riga', rep3: 'Malé', rep4: 'Monrovia',resp:'B'},
      { id: 55, quest:' du Liban?', rep1: 'Beyrouth', rep2: 'Vaduz', rep3: 'Tallinn', rep4: 'Chisinau',resp:'A'},
      { id: 56, quest:' de la Libye?', rep1: 'Antananarivo', rep2: 'Varsovie', rep3: 'La Valette', rep4: 'Tripoli',resp:'D'},
      { id: 57, quest:' du Mali?', rep1: 'Katmandou', rep2: 'Dodoma', rep3: 'Bamako', rep4: 'Beyrouth',resp:'C'},
      { id: 58, quest:' du Maroc?', rep1: 'Rabat', rep2: 'Alger', rep3: 'Tunis', rep4: 'Dakar',resp:'A'},
      { id: 59, quest:' de Malte?', rep1: 'Dublin', rep2: 'La Valette', rep3: 'Conakry', rep4: 'Amman',resp:'B'},
      { id: 60, quest:' du Mexique?', rep1: 'Tegucigalpa', rep2: 'San José', rep3: 'Banjul', rep4: 'Mexico',resp:'D'},
      { id: 61, quest:' de la Mongolie?', rep1: 'Banjul', rep2: 'Lima', rep3: 'Tbilissi', rep4: 'Oulan-Bator',resp:'D'},
      { id: 62, quest:' du Népal', rep1: 'Katmandou', rep2: 'Windhoek', rep3: 'Kigali', rep4: 'Apia',resp:'A'},
      { id: 63, quest:' du Nicaragua', rep1: 'Chisinau', rep2: 'Nouakchott', rep3: 'Managua', rep4: 'Palikir',resp:'C'},
      { id: 64, quest:' du Niger', rep1: 'Niamey', rep2: 'Abuja', rep3: 'Khartoum', rep4: 'Doha',resp:'B'},
      { id: 65, quest:' de la Norvège', rep1: 'Oslo', rep2: 'Stockholm', rep3: 'Copenhague', rep4: 'Oslo',resp:'D'},
      { id: 66, quest:' de la Nouvelle-Zélande', rep1: 'Wellington', rep2: 'Canberra', rep3: 'Freetown', rep4: 'Victoria',resp:'A'},
      { id: 67, quest:' du Paraguay', rep1: 'Lima', rep2: 'Montevideo', rep3: 'Asuncion', rep4: 'Tegucigalpa',resp:'C'},
      { id: 69, quest:' des Pays-Bas', rep1: 'Apia', rep2: 'Amsterdam', rep3: 'Bucarest', rep4: 'Londres',resp:'B'}, 
      { id: 70, quest:' du Pérou', rep1: 'Montevideo', rep2: 'Asunción', rep3: 'Managua', rep4: 'Lima',resp:'D'},   
      { id: 71, quest:' de la Pologne', rep1: 'Reykjaík', rep2: 'Skopje', rep3: 'Varsovie', rep4: 'Pristina',resp:'C'},   
      { id: 72, quest:' du Portugal', rep1: 'Lisbonne', rep2: 'Madrid', rep3: 'Dublin', rep4: 'Bucarest',resp:'A'},   
      
                                             
    ];